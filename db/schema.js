const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const crypto = require("crypto");

var schemas = {};

//
// ─── SCHEMAS ────────────────────────────────────────────────────────────────────
//

let userSchema = new Schema({
  email: String,
  role: String,
  hash: String,
  salt: String,
  created_at: { type: String, default: `${Date()}` },
  data: {},
  notifications: [
    {
      notificationId: { type: Schema.Types.ObjectId, ref: "Notification" },
      seen: { type: Boolean, default: false }
    }
  ]

});

let studentSchema = new Schema({
  userid: { type: Schema.Types.ObjectId, ref: "Users" },
  courseId: [{ type: Schema.Types.ObjectId, ref: "Courses" }],
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` },
  notifications: [
    {
      notificationId: { type: Schema.Types.ObjectId, ref: "Notification" },
      seen: { type: Boolean, default: false }
    }
  ]
});

let teacherSchema = new Schema({
  userid: { type: Schema.Types.ObjectId, ref: "Users" },
  courseId: [{ type: Schema.Types.ObjectId, ref: "Courses" }],
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` },
  notifications: [
    {
      notificationId: { type: Schema.Types.ObjectId, ref: "Notification" },
      seen: { type: Boolean, default: false }
    }
  ]
});

let messageSchema = new Schema({
  to: [{ type: Schema.Types.ObjectId, ref: "Users" }],
  from: { type: Schema.Types.ObjectId, ref: "Users" },
  subject: String,
  content: [],
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` },
  data_append: {}
});

let courseSchema = new Schema({
  coursename: String,
  price: Number,
  status: Boolean,
  facultyId: { type: Schema.Types.ObjectId, ref: "Teacher" },
  resources: [{ type: Schema.Types.ObjectId, ref: "Resources" }],
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` },
  data_append: {}
});

let moduleSchema = new Schema({
  name: String,
  subject: String,
  resources: [{ type: Schema.Types.ObjectId, ref: "Resources" }],
  facultyId: { type: Schema.Types.ObjectId, ref: "Users" },
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});

let resourcesSchema = new Schema({
  courseId: { type: Schema.Types.ObjectId, ref: "Courses" },
  name: String,
  downloadname: String,
  createdBy: { type: Schema.Types.ObjectId, ref: "Users" },
  modifiedBy: { type: Schema.Types.ObjectId, ref: "Users" },
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});

let calenderSchema = new Schema({
  userId: Schema.Types.ObjectId,
  role: String,
  className: String,
  description: String,
  courseId: Schema.Types.ObjectId,
  allDay: { type: Boolean, default: true },
  end: String,
  id: Number,
  start: String,
  title: String,
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});

let examsSchema = new Schema({
  examId: { type: Schema.Types.ObjectId, ref: "Exams" },
  examName: String,
  courseId: Schema.Types.ObjectId,
  duration: Number,
  status: String,
  questionPaperId: Schema.Types.ObjectId,
  description: String,
  data: {},
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});

let questionBankSchema = new Schema({
  name: String,
  courseId: Schema.Types.ObjectId,
  questions: [{ type: Schema.Types.ObjectId, ref: "Questions" }],
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});

let questionSchema = new Schema({
  question: String,
  optionA: String,
  optionB: String,
  optionC: String,
  optionD: String,
  answer: String,
  explaination: String,
  active: Boolean,
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});

let resultSchema = new Schema({
  questionBankId: Schema.Types.ObjectId,
  userId: { type: Schema.Types.ObjectId, ref: "Users" },
  date: String,
  examId: { type: Schema.Types.ObjectId, ref: "Exams" },
  courseId: { type: Schema.Types.ObjectId, ref: "Courses" },
  maxMarks: Number,
  obtainedMarks: Number,
  questions: [{ type: Schema.Types.ObjectId, ref: "Questions" }],
  answers: [
    {
      questionId: Schema.Types.ObjectId,
      answer: String
    }
  ],
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});

let ipHistorySchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: "Users" },
  data: {},
  created_at: { type: String, default: `${Date()}` }
});

let noticeSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: "Users" },
  data: {},
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
});
let notificationSchema = new Schema({
  createdBy: { type: Schema.Types.ObjectId, ref: "Users" },
  message: String,
  title:String,
  type: String,
  eventDate:String,
  role: String,
  courseId: { type: Schema.Types.ObjectId, ref: "Courses" },
  created_at: { type: String, default: `${Date()}` },
  updated_at: { type: String, default: `${Date()}` }
})
//
// ─── SCHEMAS OPTIONS ────────────────────────────────────────────────────────────
//

userSchema.methods.setPassword = function (password) {
  // Creating a unique salt for a particular user
  this.salt = crypto.randomBytes(16).toString("hex");

  // Hashing user's salt and password with 1000 iterations,
  // 64 length and sha512 digest
  this.hash = crypto
    .pbkdf2Sync(password, this.salt, 1000, 64, `sha512`)
    .toString(`hex`);
};
userSchema.methods.validPassword = function (password) {
  var hash = crypto
    .pbkdf2Sync(password, this.salt, 1000, 64, `sha512`)
    .toString(`hex`);
  return this.hash === hash;
};

//
// ─── MODELS ─────────────────────────────────────────────────────────────────────
//

schemas.user = mongoose.model("Users", userSchema);

schemas.course = mongoose.model("Courses", courseSchema);

schemas.message = mongoose.model("Messages", messageSchema);

schemas.module = mongoose.model("Module", moduleSchema);

schemas.resources = mongoose.model("Resources", resourcesSchema);

schemas.calender = mongoose.model("Calender", calenderSchema);

schemas.exam = mongoose.model("Exams", examsSchema);

schemas.questions = mongoose.model("Questions", questionSchema);

schemas.questionBank = mongoose.model("QuestionBank", questionBankSchema);

schemas.results = mongoose.model("Results", resultSchema);

schemas.ipHistory = mongoose.model("IpHistory", ipHistorySchema);

schemas.student = mongoose.model("Student", studentSchema);

schemas.teacher = mongoose.model("Teacher", teacherSchema);

schemas.notice = mongoose.model("Notic", noticeSchema);

schemas.notification = mongoose.model("Notification", notificationSchema);

module.exports = schemas;
