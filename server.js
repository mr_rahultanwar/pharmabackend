const express = require("express");
const listEndpoints = require('express-list-endpoints')
var bodyParser = require("body-parser");
var cors = require("cors");
const dotenv = require('dotenv');
const db = require('../../db/db')
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(cors());
dotenv.config();
const port = process.env.port || 8080;

let server = {};


app.use('/api', require('./index'));
const routes = require('../../routes')
app.get('/',(req,res)=>{
  res.send(routes)

})
//
// ─── START SERVER ───────────────────────────────────────────────────────────────
//

server.init = function() {
  app.listen(port, "0.0.0.0", () =>{
    db();
    console.log(`Server Started On : ${port}!`)
  }
  );
};

//
// ─── MOUDLE EXPORT ──────────────────────────────────────────────────────────────
//

module.exports = server;
