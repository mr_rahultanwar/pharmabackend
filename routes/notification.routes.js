const NotificationController = require('../controllers/NotificationController')
const notificationcontroller = new NotificationController()
module.exports = [

    {
        path: '/notification/create',
        method: 'post',
        controller: notificationcontroller.createNotification,
        request: {
            createdBy: "userid of creater",
            message: "message",
            type: "nature of notification",
            role: "role of the sender",
            courseId: "courserid",
        },
        response: {
            "callback": "body",
            "status": "200"
        }
    }
]