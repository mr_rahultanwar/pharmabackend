const TeachersController = require("../controllers/TeachersController");
const teacherscontroller = new TeachersController();
module.exports = [
  {
    path: "/teacher/get",
    method: "get",
    controller: teacherscontroller.getTeachers,
    request: {},
    response: {
      stauts: "True or False",
      teacher: "teacher data []",
      message: "Network Error!"
    }
  },
  {
    path: "/teacher/get/id/:id",
    method: "get",
    controller: teacherscontroller.getTeacherById,
    request: {},
    response: {
      stauts: "True or False",
      teacher: "teacher data []",
      message: "Network Error!"
    }
  },
  {
    path: "/teacher/updateById",
    method: "post",
    controller: teacherscontroller.updateTeacherById,
    request: {
      id:"user id",
      notifications:"[]"
    },
    response: {
      stauts: "True or False",
      teacher: "teacher data []",
      message: "Network Error!"
    }
  },
  {
    path: "/teacher/update/module",
    method: "post",
    controller: teacherscontroller.updateTeacherModule,
    request: {
      id: "Teacher Obj id",
      modules: "Teacher Modules []"
    },
    response: {
      status: "true or false",
      message: "Modules Added or network error"
    }
  }
];
