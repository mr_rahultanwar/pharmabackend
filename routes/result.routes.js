const ResultController = require("../controllers/ResultController");
const resultcontroller = new ResultController();
module.exports = [
  {
    path: "/result/create",
    method: "post",
    controller: resultcontroller.resultCreate,
    request: {
      examId:"Exam Id",
      courseId:"Course id",
      questionBankId: "Question Bank Id",
      userId: "User id",
      date: "Date of result",
      maxMarks: "Max Marks",
      obtainedMarks: "Obtained marks ",
      questions: "Questions []",
      answers: "Answers []"
    },
    response: {
      stauts: "True or False",
      message: "Result Saved / Network Error!"
    }
  },
  {
    path: "/result/update",
    method: "post",
    controller: resultcontroller.resultUpdate,
    request: {
      questionBankId: "Question Bank Id",
      userId: "User id",
      date: "Date of result",
      maxMarks: "Max Marks",
      obtainedMarks: "Obtained marks ",
      questions: "Questions []",
      answers: "Answers []"
    },
    response: {
      stauts: "True or False",
      message: "Result Saved / Network Error!"
    }
  },
  {
    path: "/result/get",
    method: "get",
    controller: resultcontroller.getResult,
    request: {},
    response: {
      stauts: "True or False",
      teacher: "results []",
      message: "Network Error!"
    }
  },
  {
    path: "/result/get/id/:id",
    method: "get",
    controller: resultcontroller.getResultById,
    request: {
      ":id": "Result Objid"
    },
    response: {
      stauts: "True or False",
      teacher: "results []",
      message: "Network Error!"
    }
  },
  {
    path: "/result/get/course/:id",
    method: "get",
    controller: resultcontroller.getResultByCourseId,
    request: {
      ":id": "course Objid"
    },
    response: {
      stauts: "True or False",
      teacher: "results []",
      message: "Network Error!"
    }
  },
  {
    path: "/result/get/userId/:id",
    method: "get",
    controller: resultcontroller.getResultByUserId,
    request: {
      ":id": "course Objid"
    },
    response: {
      stauts: "True or False",
      teacher: "results []",
      message: "Network Error!"
    }
  }
];
