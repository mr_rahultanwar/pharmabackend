const ModuleController = require("../controllers/ModuleController");
const modulecontroller = new ModuleController();
module.exports = [
  {
    path: "/module/create",
    method: "post",
    controller: modulecontroller.createModule,
    request: {
      name: "Module Name",
      subject: "Module Subject",
      resources: "Resources []",
      facultyId: "faculty Obj id"
    },
    response: {
      callback: "body",
      status: "200"
    }
  },
  {
    path: "/module/get",
    method: "get",
    controller: modulecontroller.getAllModule,
    request: {},
    response: {
      status: "true or false",
      modules: "Modules []"
    }
  },
  {
    path: "/module/get/id/:id",
    method: "get",
    controller: modulecontroller.getAllModuleById,
    request: {
      ":id":"Module Id"
    },
    response: {
      status: "true or false",
      modules: "Modules []"
    }
  },
  {
    path: "/module/update",
    method: "post",
    controller: modulecontroller.updateModule,
    request: {
      name: "Name",
      subject: "Subject",
      resources: "Resources []",
      facultyId: "faculty id"
    },
    response: {
      status: "true or false",
      message: "Module Updated"
    }
  }
];
