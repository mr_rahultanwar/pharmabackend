const MessagesController = require("../controllers/MessagesController");
const messagescontroller = new MessagesController();
module.exports = [
  {
    path: "/messages/create",
    method: "post",
    controller: messagescontroller.createMessage,
    request: {
      to: "[] of obj_ids",
      from: "senders obj_id",
      subject: "Subject String",
      content: "Content String",
      data_append: "Data to Append"
    },
    response: {
      callback: "body",
      status: "200"
    }
  },
  {
    path: "/messages/get/recieved/:id",
    method: "get",
    controller: messagescontroller.getRecivedMessage,
    request: {
      from: "Senders obj id"
    },
    response: {
      status: "true of false",
      message: "messages []"
    }
  },
  {
    path: "/messages/get/sent/:id",
    method: "get",
    controller: messagescontroller.getSentMessage,
    request: {
      from: "Senders obj id"
    },
    response: {
      status: "true of false",
      message: "messages []"
    }
  },
  {
    path: "/messages/get/id/:id",
    method: "get",
    controller: messagescontroller.getMessageById,
    request: {
      from: "Message obj id"
    },
    response: {
      status: "true of false",
      message: "messages []"
    }
  }
];
