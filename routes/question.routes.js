const QuestionController = require("../controllers/QuestionController");
const questioncontroller = new QuestionController();
module.exports = [
  {
    path: "/question/create",
    method: "post",
    controller: questioncontroller.createQuestion,
    request: {
      question: "Question ?",
      optionA: "Option A",
      optionB: "Option B",
      optionC: "Option C",
      optionD: "Option D",
      answer: "Correct Option [optionA,optionB,optionC,optionD]",
      explaination: "Question Explaination",
      active: "Question Status [true or false]"
    },
    response: {
        status:"true or false"
    }
  }
];
