const ResourceController = require("../controllers/ResourceController");
const resourcecontroller = new ResourceController();
module.exports = [
  {
    path: "/resource/create",
    method: "post",
    controller: resourcecontroller.createResource,
    request: {
      moduleId: "Module Id",
      name: "Resource Name",
      
    },
    response: {
      callback: "body",
      status: "200"
    }
  },
  {
    path: "/resource/get/:id",
    method: "get",
    controller:resourcecontroller.getResourseById ,
    request: {},
    response: {
      status:"true/false",
      resource:"resource []",
    }
  },
  {
    path: "/resource/delete/:id",
    method: "get",
    controller:resourcecontroller.deleteResources ,
    request: {},
    response: {
      status:"true/false",
      resource:"resource []",
    }
  },
  {
    path: "/resource/download/get/:id",
    method: "get",
    controller:resourcecontroller.downloadFileById ,
    request: {},
    response: {
      status:"true/false",
      file:"resource []",
    }
  }
];
