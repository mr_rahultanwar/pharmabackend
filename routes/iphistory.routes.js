const IpHistoryController = require("../controllers/IpHistoryController");
const iphistorycontroller = new IpHistoryController();
module.exports = [
  {
   path: "/iphistory",
   method: "post",
   controller: iphistorycontroller.saveIp,
   request: {
    id:"User Id",
    data:"User Ip Data"
   },
   response: {
       status:"true or false",
       message:"Status Message"
   }
  },
  {
      path: "/iphistory/get",
      method: "get",
      controller: iphistorycontroller.getIp,
      request: {},
      response: {
          status:"true or false",
          message:"ips or network error!"
      }
  }
];
