const NoticeController = require("../controllers/NoticeController");
const noticecontroller = new NoticeController();
module.exports = [
  {
    path: "/notice/create",
    method: "post",
    controller: noticecontroller.noticeCreate,
    request: {
      userId: "User Obj id",
      data: "Data {}"
    },
    response: {
      status: "true or false",
      message: "Notice Created / Network Error!"
    }
  },
  {
    path: "/notice/get",
    method: "get",
    controller: noticecontroller.getNotice,
    request: {},
    response: {
      status: "true or false",
      notice: "Notice -  []"
    }
  },
  {
    path: "/notice/get/id/:id",
    method: "get",
    controller: noticecontroller.getNoticeById,
    request: {},
    response: {
      status: "true or false",
      notice: "Notice []"
    }
  },
  {
    path: "/notice/delete/:id",
    method: "get",
    controller: noticecontroller.deleteNoticeById,
    request: {
      ":id": "Notice Obj Id"
    },
    response: {
      status: "true or false",
      message: "Notice Created / Network Error!"
    }
  }
];
