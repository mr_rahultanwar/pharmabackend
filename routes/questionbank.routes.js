const QuestionBankController = require("../controllers/QuestionBankController");
const questionbankcontroller = new QuestionBankController();
module.exports = [
  {
    path: "/questionbank/get",
    method: "get",
    controller: questionbankcontroller.getBank,
    request: {},
    response: {
      status: "true or false",
      questions: "Questions []",
      message: "Network Error!"
    }
  },
  {
    path: "/questionbank/get/id/:id",
    method: "get",
    controller: questionbankcontroller.getBankById,
    request: {
      ":id":"Question Bank Id"
    },
    response: {
      status: "true or false",
      questions: "Question {}",
      message: "Network Error!"
    }
  },
  {
    path: "/questionbank/courseId/id/:id",
    method: "get",
    controller: questionbankcontroller.getBankByCourseId,
    request: {
      ":id":"Question Bank Id"
    },
    response: {
      status: "true or false",
      questions: "Question {}",
      message: "Network Error!"
    }
  },
  {
    path: "/questionbank/create",
    method: "post",
    controller: questionbankcontroller.saveBank,
    request: {
      name: "Question Bank Name",
      courseId: "Course Id",
      questions: "Questions []"
    },
    response: {
      status: "true or false",
      message: "Question Bank Saved or Network Error",
      id: "Ob Id"
    }
  }
];
