const UserController = require("../controllers/UserController");
const usercontroller = new UserController();
module.exports = [
  {
    path: "/user/login",
    method: "post",
    controller: usercontroller.login,
    request: {
      email: "User Email",
      password: "User Password"
    },
    response: {
      status: "Response Status",
      message: "Response Message"
    }
  },
  {
    path: "/user/register",
    method: "post",
    controller: usercontroller.register,
    request: {
      email: "User Email",
      password: "User Password"
    },
    response: {
      status: "Response Status",
      message: "Response Message"
    }
  },
  {
    path: "/user/update",
    method: "post",
    controller: usercontroller.update,
    request: {
      id: "User OId",
      data: "Data {} of the user"
    },
    response: {
      status: "Response Status",
      message: "Response Message"
    }
  },
  {
    path: "/user/get/id/:id",
    method: "get",
    controller: usercontroller.getUserById,
    request: {
      id: "User Obj Id"
    },
    response: {
      status: "true or false",
      user: "User details",
      message: "User Found or not"
    }
  },
  {
    path: "/user/get",
    method: "get",
    controller: usercontroller.getUser,
    request: {},
    response: {
      status: "Response Status",
      users: "users []"
    }
  },
  {
    path: "/user/get/:role",
    method: "get",
    controller: usercontroller.getUserByRole,
    request: {
      role: "User Role"
    },
    response: {
      status: "Response Status",
      users: "users []"
    }
  },
  {
    path: "/user/emails/get",
    method: "get",
    controller: usercontroller.getAllUsersEmailId,
    request: {},
    response: {
      status: "Response Status",
      users: "users []"
    }
  }
];
