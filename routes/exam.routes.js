const ExamController = require("../controllers/ExamController");
const examcontroller = new ExamController();
module.exports = [
  {
   path: "/exam/create",
   method: "post",
   controller:examcontroller.createExam ,
   request: {
    
       
        examName: "exam name",
        courseId: "course object id",
        duration: "string duration",
        status: "true false active/inactive",
        questionPaperId: "question bank id",
        description: "description",
        data: "{other details}"
    
   },
   response: {
       status:"true/false",
       message:"message",
       
   }
  },
  {
    path: "/exam/get",
    method: "get",
    controller:examcontroller.getExam ,
    request: {},
    response: {
      status:"true/false",
      exam:"Exam []",
    }
  },
  {
    path: "/exam/get/courseid/:id",
    method: "get",
    controller:examcontroller.getExamByCourseId ,
    request: {},
    response: {
      status:"true/false",
      exam:"Exam []",
    }
  },
  {
    path: "/exam/get/id/:id",
    method: "get",
    controller:examcontroller.getExamByExamId ,
    request: {},
    response: {
      status:"true/false",
      exam:"Exam []",
    }
  },


];
