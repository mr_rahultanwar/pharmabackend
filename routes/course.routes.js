const CourseController = require("../controllers/CourseController");
const coursecontroller = new CourseController();
module.exports = [
  {
    path: "/course/create",
    method: "post",
    controller: coursecontroller.saveCourse,
    request: {
      coursename: "coursename",
      batch: "batch",
      price: "price",
      status: "status",
      modules: "modules",
      data_append: "data_append"
    },
    response: {
      callback: "body",
      status: "200"
    }
  },
  {
    path: "/course/get",
    method: "get",
    controller: coursecontroller.getAllCourse,
    request: {},
    response: {
      status: "true or false",
      courses: "courses []"
    }
  },
  {
    path: "/course/get/id/:id",
    method: "get",
    controller: coursecontroller.getCourseById,
    request: {
      id: "Obj Id"
    },
    response: {
      status: "true or false",
      courses: "courses []"
    }
  },
  {
    path: "/course/update",
    method: "post",
    controller: coursecontroller.updateCouserById,
    request: {
      id:'Obj Id',
      coursename: "Couser Name",
      batch: "Batch Name",
      price: "Batch Pirce",
      status: "Batch Status",
      modules: "Batch Modules",
      data_append: "Some Extra Data",
    },
    response: {
      status: "true or false",
      message:"Query message"
    }
  }
];
