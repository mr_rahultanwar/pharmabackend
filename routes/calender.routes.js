const CalenderController = require("../controllers/CalenderController");
const calendercontroller = new CalenderController();
module.exports = [
  {
    path: "/calender/create",
    method: "post",
    controller: calendercontroller.saveCalender,
    request: {
      userid: "User id ",
      courseid: "Course id",
      className: "Class Name",
      description: "Description",
      end: "End Date",
      id: "Event auto Id",
      start: "Start Date",
      title: "Event Date",
      role: "User role"
    },
    response: { status: "true or false", message: "Event Message" }
  },
  {
    path: "/calender/get",
    method: "get",
    controller: calendercontroller.getCalender,
    request: {},
    response: { status: "true or false", event: "Events []" }
  },
  {
    path: "/calender/update",
    method: "post",
    controller: calendercontroller.updateCalender,
    request: {
      courseId: "Course Id",
      description: "Event Description ",
      title: "Event Title"
    },
    response: {}
  },
  {
    path: "/calender/delete/:id",
    method: "get",
    controller: calendercontroller.deleteCalender,
    request: {
      ":id": "Calender Id"
    },
    response: {}
  },
  {
    path: "/calender/get/course/:id",
    method: "get",
    controller: calendercontroller.getCalenderByCourseId,
    request: {
      ":id": "course id"
    },
    response: {}
  }
];
