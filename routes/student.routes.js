const StudentController = require("../controllers/StudentController");
const studentcontroller = new StudentController();
module.exports = [
  {
    path: "/student/get",
    method: "get",
    controller: studentcontroller.getStudents,
    request: {},
    response: {
      stauts: "True or False",
      student: "student data []",
      message: "Network Error!"
    }
  },
  {
    path: "/student/get/id/:id",
    method: "get",
    controller: studentcontroller.getStudentById,
    request: {},
    response: {
      stauts: "True or False",
      student: "student data []",
      message: "Network Error!"
    }
  },
  {
    path: "/student/get/courseId/:id",
    method: "get",
    controller: studentcontroller.getCalenderByCourseId ,
    request: {},
    response: {
      stauts: "True or False",
      student: "student data []",
      message: "Network Error!"
    }
  },
  {
    path: "/student/updateById",
    method: "post",
    controller: studentcontroller.updateStudentById,
    request: {
      id:"user id",
      notifications:"[]"
    },
    response: {
      stauts: "True or False",
      teacher: "teacher data []",
      message: "Network Error!"
    }
  },
  {
    path: "/student/update/course",
    method: "post",
    controller: studentcontroller.updateStudentCourse,
    request: {
      courses: "courses []"
    },
    response: {
      status: "true or false",
      message: "Modules Added or network error"
    }
  }
];
