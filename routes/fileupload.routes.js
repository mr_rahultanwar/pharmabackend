const FileUploadController = require("../controllers/FileUploadController");
const fileuploadcontroller = new FileUploadController();
module.exports = [
  {
    path: "/upload/resources",
    method: "post",
    controller: fileuploadcontroller.uploadResoures,
    request: {
      file: "File",
      moduleid: "Module Id",
      name: "Resource Name",
      createdBy: "Created User Id"
    },
    response: {
      status: "true or false",
      message: "File uploaded or Network Error!",
      resourceId: "Successfull File Resource id"
    }
  }
];
