let QuestionBank = require("../db/schema").questionBank;
class QuestionBankController {
  async saveBank(req, res) {
    let questionbank = new QuestionBank({
      name: req.body.name,
      courseId: req.body.courseId,
      questions: req.body.questions
    });

    await questionbank
      .save()
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Question Bank Saved",
          id: docs._id
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getBank(req, res) {
    QuestionBank.find({}).populate('questions')
      .then(docs => {
        return res.status(200).send({
          status: true,
          questions: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error"
        });
      });
  }

  async getBankById(req, res) {
    QuestionBank.findById(req.params.id).populate('questions')
      .then(docs => {
        return res.status(200).send({
          status: true,
          question: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error"
        });
      });
  }

  async getBankByCourseId(req, res) {
    QuestionBank.find({courseId:req.params.id}).populate('questions')
      .then(docs => {
        return res.status(200).send({
          status: true,
          question: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error"
        });
      });
  }
}
module.exports = QuestionBankController;
