const Student = require("../db/schema").student;
class StudentController {
  async getStudents(req, res) {
    Student.find({})
      .populate("userid")
      .populate("courseId")
      .then(docs => {
        return res.status(200).send({
          status: true,
          Students: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getStudentById(req, res) {
    Student.findById(req.params.id)
      .populate("userid")
      .populate("courseId").populate('notifications.notificationId')
      .then(docs => {
        return res.status(200).send({
          status: true,
          Student: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
  async getCalenderByCourseId(req, res) {
    Student.find({ courseId: req.params.id })
    .populate("userid")
    .populate("courseId")
      .then(docs => {
        return res.status(200).send({
          status: true,
          students: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async updateStudentCourse(req, res) {
    Student.findByIdAndUpdate(req.body.id, {
      $set: { courseId: req.body.courses }
    })
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Course Added"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
  async updateStudentById(req, res) {
    Student.findByIdAndUpdate(req.body.id,{'$set':{'notifications':req.body.notifications}})
      .then(docs => {
        Student.find({}).then(te=>{
          return res.status(200).send({
            status: true,
            student: te
          });
        })
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = StudentController;
