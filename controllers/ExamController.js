const Exam = require("../db/schema").exam;
class ExamController {
  async createExam(req, res) {

    let exam = Exam({
        
        examName: req.body.examname,
        courseId: req.body.courseId,
        duration: req.body.duration,
        status: req.body.status,
        questionPaperId: req.body.questionPaperId,
        description: req.body.description,
        data: req.body.data
    })
    exam.save().then(docs => {
    return res.status(200).send({
        status:true,
        message:"exam created"
    });
    })
     .catch(err => {
     return res.status(400).send({
         status:false,
         message:"network error"
     });
    });
  }

  async getExam(req,res){
    Exam.find({}).then(docs => {
    return res.status(200).send({
      status:true,
      exam:docs
    });
    })
     .catch(err => {
     return res.status(400).send({ 
    status: false,
    message: "Network Error!" 
    });
    });
  }

  async getExamByCourseId(req,res){
    Exam.find({"courseId":req.params.id}).then(docs=>{
      return res.status(200).send({
        status:true,
        exam:docs
      })
    }).catch(err=>{

    })
  }
  async getExamByExamId(req,res){
    Exam.findById(req.params.id).then(docs=>{
      return res.status(200).send({
        status:true,
        exam:docs
      })
    }).catch(err=>{

    })
  }
}
module.exports = ExamController;
