const Question = require("../db/schema").questions;
class QuestionController {
  async createQuestion(req, res) {
    let questions = new Question({
      question: req.body.question,
      optionA: req.body.optionA,
      optionB: req.body.optionB,
      optionC: req.body.optionC,
      optionD: req.body.optionD,
      answer: req.body.answer,
      explaination: req.body.explaination,
      active: req.body.active
    });

    await questions
      .save()
      .then(docs => {
        return res.status(200).send({
          status: true,
          id: docs._id,
          question: questions
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = QuestionController;
