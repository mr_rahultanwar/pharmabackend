const Calender = require("../db/schema").calender;
class CalenderController {
  async saveCalender(req, res) {
    let calender = new Calender({
      userId: req.body.userid,
      className: req.body.className,
      courseId: req.body.courseid,
      description: req.body.description,
      end: req.body.end,
      id: req.body.id,
      start: req.body.start,
      title: req.body.title,
      role: req.body.role
    });

    let c = await calender
      .save()
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Event Added"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error"
        });
      });
  }

  async getCalender(req, res) {
    Calender.find({})
      .then(docs => {
        return res.status(200).send({
          status: true,
          events: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error"
        });
      });
  }

  async updateCalender(req, res) {
    Calender.findByIdAndUpdate(req.body.id, {
      $set: {
        courseId:req.body.courseId,
        description: req.body.description,
        title: req.body.title
      }
    })
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Calender Update"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getCalenderByCourseId(req, res) {
    Calender.find({ courseId: req.params.id })
      .then(docs => {
        return res.status(200).send({
          status: true,
          events: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async deleteCalender(req, res) {
    Calender.findByIdAndDelete(req.params.id)
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Event Deleted!"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = CalenderController;
