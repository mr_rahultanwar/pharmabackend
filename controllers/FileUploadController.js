const path = require("path");
const Resources = require("../db/schema").resources;
const Course = require("../db/schema").course;
class FileUploadController {
  async index(req, res) {
    res.send(req.body);
  }

  async uploadResoures(req, res) {
    if (!req.files || Object.keys(req.files).length === 0) {
      return res.status(400).send("No files were uploaded.");
    }

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let sampleFile = req.files.file;

    let filename = `${Date.now()}.${sampleFile.name}`
    let resourcePath = path.join(
      __dirname,
      "../uploads/resources/",
        filename
    );


    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(resourcePath,async function(err) {
      if (err) return res.status(500).send(err);
      let resources = new Resources({
        courseId: req.body.courseId,
        name: req.body.name,
        downloadname:filename,
        createdBy: req.body.createdBy,
        modifiedBy: req.body.createdBy
      });

    let r = await resources
    .save()
    .then(resource_docs => {
      Course.findById(req.body.courseId)
        .then(docs => {
          docs.resources.push(resource_docs._id);
          docs.save();
          return res.status(200).send({
            status: true,
            Resourse: docs.resources,
            message: "Resource Created"
          });
        })
        .catch(err => {
          return res.status(400).send({
            status: false,
            message: "Network Error!"
          });
        });
    })
    .catch(err => {
      return res.status(400).send({
        status: false,
        message: "Resource Not Created"
      });
    });
      
    });
  }

  
}
module.exports = FileUploadController;
