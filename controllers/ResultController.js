const Result = require("../db/schema").results;
class ResultController {
  async resultCreate(req, res) {
    let result = new Result({
      examId: req.body.examId,
      courseId: req.body.courseId,
      questionBankId: req.body.questionBankId,
      userId: req.body.userId,
      date: req.body.date,
      maxMarks: req.body.maxMarks,
      obtainedMarks: req.body.obtainedMarks,
      questions: req.body.questions,
      answers: req.body.answers
    });

    result
      .save()
      .then(docs => {
        return res.status(200).send({
          resultId: docs._id,
          status: true,
          message: "Result Submitted"
        });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async resultUpdate(req, res) {
    Result.findByIdAndUpdate(req.body.id)
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Result Updated"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getResult(req, res) {
    Result.find({})
      .populate("examId")
      .populate("userId")
      .populate("courseId")
      .then(docs => {
        return res.status(200).send({
          status: true,
          results: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getResultById(req, res) {
    Result.find({})
      .then(docs => {
        return res.status(200).send({
          status: true,
          results: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getResultByCourseId(req, res) {
    Result.find({ courseId: req.params.id })
      .then(docs => {
        return res.status(200).send({
          status: true,
          results: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
  async getResultByUserId(req, res) {
    Result.find({ userId: req.params.id })
    .populate("examId")
      .populate("userId")
      .populate("courseId")
      .then(docs => {
        return res.status(200).send({
          status: true,
          results: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = ResultController;
