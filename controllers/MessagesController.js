const Message = require("../db/schema").message;

class MessagesController {
  async createMessage(req, res) {
    let message = new Message({
      to: req.body.to,
      from: req.body.from,
      subject: req.body.subject,
      content: req.body.content,
      data_append: req.body.data_append
    });

    let m = await message
      .save()
      .then(message_res => {
        res.status(200).send({
          status: true,
          message: "Message Sent"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(401).send({
          status: false,
          message: "Network Error!"
        });
      });

    return m;
  }

  async getRecivedMessage(req, res) {
    Message.find({ to: req.params.id })
      .populate("from")
      .then(message_res => {
        res.status(200).send({
          status: true,
          message: message_res
        });
      })
      .catch(err => {
        console.log(err);
        res.status(401).send({
          status: false,
          message: []
        });
      });
  }

  async getSentMessage(req, res) {
    Message.find({ from: req.params.id }).populate('to')
      .then(message_res => {
        res.status(200).send({
          status: true,
          message: message_res
        });
      })
      .catch(err => {
        console.log(err);
        res.status(401).send({
          status: false,
          message: []
        });
      });
  }

  async getMessageById(req, res) {
    Message.findById(req.params.id)
      .then(message_res => {
        res.status(200).send({
          status: true,
          message: message_res
        });
      })
      .catch(err => {
        console.log(err);
        res.status(401).send({
          status: false,
          message: []
        });
      });
  }
}
module.exports = MessagesController;
