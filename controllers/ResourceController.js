const Resource = require("../db/schema").resources;
const Course = require("../db/schema").course;
const path = require("path");
class ResourceController {
  async createResource(req, res) {
    let resource = new Resource({
      courseId: req.body.courseId,
      name: req.body.name
    });

    let r = await resource
      .save()
      .then(resource_docs => {
        Course.findById(req.body.courseId)
          .then(docs => {
            docs.resources.push(req.body.courseId);
            docs.save();
            return res.status(200).send({
              status: true,
              _id: docs.id,
              message: "Resource Created"
            });
          })
          .catch(err => {
            return res.status(400).send({
              status: false,
              message: "Network Error!"
            });
          });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Resource Not Created"
        });
      });
  }
  async getResourseById(req, res) {
    Resource.findById(req.params.id)
      .then(docs => {
        return res.status(200).send({
          status: true,
          resource: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "User Not Found!"
        });
      });
  }
  async downloadFileById(req, res) {
    let downloadPath = path.join(__dirname, "../", "uploads", "resources");

    Resource.findById(req.params.id).then(doc => {
      return res.download(`${downloadPath}/${doc.downloadname}`);
    });
  }

  async deleteResources(req, res) {

    let r = await Resource.findById(req.params.id).then((res)=>{return res})
    console.log(r)
    Resource.findByIdAndDelete(req.params.id)
      .then(resource_docs => {
        Course.findById(r.courseId)
          .then(course_doc => {
            let newcourse = course_doc.resources.filter(
              x => x != req.params.id
            );
            course_doc.resources = newcourse;
            course_doc
              .save()
              .then(docs => {
                return res.status(200).send({
                  status: true,
                  message: "Resource Delete"
                });
              })
              .catch(err => {
                console.log(err);
                return res.status(400).send({
                  status: false,
                  message: "Network Error!"
                });
              });
          })
          .catch(err => {
            console.log(err);

            return res.status(400).send({
              status: false,
              message: "Network Error!"
            });
          });
      })
      .catch(err => {
        console.log(err);

        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = ResourceController;
