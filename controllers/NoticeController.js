const Notice = require("../db/schema").notice;
class NoticeController {
  async noticeCreate(req, res) {
    let notice = new Notice({
      userId: req.body.userid,
      data: req.body.data
    });

    notice
      .save()
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Notic Created!"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getNotice(req, res) {
    Notice.find({})
      .then(docs => {
        return res.status(200).send({
          status: true,
          notice: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getNoticeById(req, res) {
    Notice.findById(req.params.id)
      .then(docs => {
        return res.status(200).send({
          status: true,
          notice: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        }); 
      });
  }

  async deleteNoticeById(req, res) {
    Notice.findByIdAndDelete(req.params.id)
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Notice Deleted!"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = NoticeController;
