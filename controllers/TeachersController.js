const Teacher = require("../db/schema").teacher;
class TeachersController {
  async getTeachers(req, res) {
    Teacher.find({}).populate('userid').populate('courseId')
      .then(docs => {
        return res.status(200).send({
          status: true,
          teachers: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getTeacherById(req, res) {
    Teacher.findById(req.params.id).populate('userid').populate('courseId').populate('notifications.notificationId')
      .then(docs => {
        return res.status(200).send({
          status: true,
          teacher: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
  async updateTeacherById(req, res) {
    Teacher.findByIdAndUpdate(req.body.id,{'$set':{'notifications':req.body.notifications}})
      .then(docs => {
        Teacher.find({}).then(te=>{
          return res.status(200).send({
            status: true,
            teacher: te
          });
        })
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async updateTeacherModule(req, res) {
    Teacher.findByIdAndUpdate(req.body.id, {
      $set: { courseId: req.body.courseId }
    })
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Module Added"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = TeachersController;
