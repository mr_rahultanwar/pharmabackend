const User = require("../db/schema").user;
const Student = require("../db/schema").student;
const Teacher = require("../db/schema").teacher;
class UserController {
  async login(req, res) {
    // Find user with requested email

    User.findOne({ email: req.body.email }, function (err, user) {
      console.log(err);
      console.log(user);
      if (user === null) {
        return res.status(400).send({
          status: false,
          message: "User not found."
        });
      } else {
        try {
          if (user.validPassword(req.body.password)) {
            return res.status(201).send({
              status: true,
              message: "User Logged In",
              user: user
            });
          } else {
            return res.status(400).send({
              status: false,
              message: "Wrong Password"
            });
          }
        } catch (err) {
          console.log(err);
          return res.status(500).send({
            status: false,
            message: "Server Error"
          });
        }
      }
    });
  }

  async register(req, res) {
    // Creating empty user object
    let newUser = new User();

    // Intialize newUser object with request data
    (newUser.name = req.body.name),
      (newUser.email = req.body.email),
      (newUser.role = req.body.role);
    newUser.data = req.body.data;

    // Call setPassword function to hash password
    newUser.setPassword(req.body.password);

    let user = await newUser
      .save()
      .then(async user_response => {
        let student = new Student({
          _id: user_response._id,
          userid: user_response._id
        });

        let teacher = new Teacher({
          _id: user_response._id,
          userid: user_response._id
        });

        if (req.body.role === "student") {
          console.log(req.body.role)

          await student.save();
        } else if (req.body.role === "teacher") {
          console.log(req.body.role)
          await teacher.save();
        }
        return res.status(201).send({
          status: true,
          message: "User added succesfully.",
          userId:user_response._id
        });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).send({
          status: false,
          message: "Failed to add user."
        });
      });

    return user;
  }

  async update(req, res) {
    let user = User.findByIdAndUpdate(req.body.id, {
      $set: { data: req.body.data }
    })
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "User Data Updated"
        });
      })
      .catch(err => {
        console.log(err);
        return res.status(400).send({
          status: false,
          message: "Failed To Update User Data"
        });
      });

    return user;
  }

  async getUserById(req, res) {
    User.findById(req.params.id)
      .then(docs => {
        return res.status(200).send({
          status: true,
          user: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "User Not Found!"
        });
      });
  }

  async getUserByRole(req, res) {
    let user = User.find({ role: req.params.role })
      .then(user_res => {
        return res.status(200).send({
          status: true,
          users: user_res
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          users: []
        });
      });

    return user;
  }

  async getUser(req, res) {
    console.log("hkswdha");
    let user = User.find({})
      .then(user_res => {
        return res.status(200).send({
          status: true,
          users: user_res
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          users: []
        });
      });

    return user;
  }

  async getAllUsersEmailId(req, res) {
    let user = await User.find({}, { _id: 1, email: 1 })
      .then(user_res => {
        let u = [];
        user_res.forEach(ele => {
          u.push({
            value: ele._id,
            text: ele.email
          });
        });
        return res.status(200).send({
          status: true,
          users: u
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          users: []
        });
      });

    //  return user
  }
}
module.exports = UserController;
