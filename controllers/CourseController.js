const Course = require("../db/schema").course;
const Teacher = require("../db/schema").teacher;
class CourseController {
  async saveCourse(req, res) {
    console.log(req.body);
    let course = new Course({
      coursename: req.body.coursename,
      facultyId: req.body.facultyId,
      batch: req.body.batch,
      price: req.body.price,
      status: req.body.status,
      modules: req.body.modules,
      data_append: req.body.data_append
    });

    let c = await course
      .save()
      .then(course_docs => {
        Teacher.findById(req.body.facultyId)
          .then(docs => {
            docs.courseId.push(course_docs._id);
            docs
              .save()
              .then(docs => {
                return res.status(200).send({
                  status: true,
                  id:course_docs._id,
                  message: "Course Created"
                });
              })
              .catch(err => {
                return res.status(400).send({
                  status: false,
                  message: "Network Error!"
                });
              });
          })
          .catch(err => {
            return res.status(400).send({
              status: false,
              message: "Network Error!"
            });
          });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error"
        });
      });

    return c;
  }

  async getAllCourse(req, res) {
    let courses = await Course.find({})
      .populate("resources")
      .then(course_res => {
        res.status(200).send({
          status: true,
          courses: course_res
        });
      })
      .catch(err => {
        console.log(err);
        res.status(401).send({
          status: false,
          courses: []
        });
      });

    return courses;
  }

  async getCourseById(req, res) {
    let courses = await Course.find({ _id: req.params.id })
      .populate("resources  ")
      .then(course_res => {
        res.status(200).send({
          status: true,
          courses: course_res
        });
      })
      .catch(err => {
        res.status(401).send({
          status: false,
          courses: []
        });
      });

    return courses;
  }

  async updateCouserById(req, res) {
    Course.findByIdAndUpdate(req.body.id, {
      coursename: req.body.coursename,
      facultyId: req.body.facultyId,
      batch: req.body.batch,
      price: req.body.price,
      status: req.body.status,
      resources: req.body.resources,
      data_append: req.body.data_append,
      updated_at: `${Date()}`
    })
      .then(course_docs => {
        Teacher.findById(req.body.facultyId)
          .then(docs => {
            if (!docs.courseId.includes(course_docs._id)) {
              docs.courseId.push(course_docs._id);
              docs.save();
            }

            return res.status(200).send({
              status: true,
              message: "Data Updated"
            });
          })
          .catch(err => {
            return res.status(400).send({
              status: false,
              message: "Network Error!"
            });
          });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error"
        });
      });
  }
}
module.exports = CourseController;
