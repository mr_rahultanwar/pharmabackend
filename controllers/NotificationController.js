
const Notification = require('../db/schema').notification
const Student = require('../db/schema').student
const Teacher = require('../db/schema').teacher


class NotificationController {
  async createNotification(req, res) {
    let notification = new Notification({
      createdBy: req.body.createdBy,
      message: req.body.message,
      type: req.body.type,
      title: req.body.title,
      eventDate:req.body.eventDate,
      role: req.body.role,
      courseId: req.body.courseId,
    })

    
    await notification.save().then(async(data) => {
     await Teacher.find({ courseId: req.body.courseId }).then(teacher_doc => {
        
        let obj = {
          notificationId: data._id,
          seen: false
        }
        const forLoop = async () => {
          console.log('Start')
        
          for (let index = 0; index < teacher_doc.length; index++) {
            const noti = teacher_doc[index]['notifications']
            noti.push(obj)
           const s = await teacher_doc[index].save().then(res=>{return res})
        
          }
        
          console.log('End')
        }
        // x.notifications.push(obj)
        // teacher_doc.save()
        forLoop()
      })
     await Student.find({ courseId: req.body.courseId }).then(student_doc => {
        
        let obj = {
          notificationId: data._id,
          seen: false
        }
        const forLoop1 = async () => {
          console.log('Start')
        
          for (let index = 0; index < student_doc.length; index++) {
            const noti = student_doc[index]['notifications']
            noti.push(obj)
           const s = await student_doc[index].save().then(res=>{return res})
        
          }
        
          console.log('End')
        }
        // x.notifications.push(obj)
        // teacher_doc.save()
        forLoop1()
      })

      res.status(200).send({
        status: true,
        id: data._id,

        notification: "notification created"
      })
    }).catch(err => {
      console.log(err);
      res.status(401).send({
        status: false,
        message: "Network Error!"
      });
    })


  }
}
module.exports = NotificationController;