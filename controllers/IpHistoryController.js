const IpHistory = require("../db/schema").ipHistory;
class IpHistoryController {
  async saveIp(req, res) {
    let iphistory = new IpHistory({
      userId: req.body.id,
      data: req.body.data
    });

    await iphistory
      .save()
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Main Bhagwan hu mujhe sab pata"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }

  async getIp(req, res) {
    await IpHistory.find({}).populate('userId')
      .then(docs => {
        return res.status(200).send({
          status: true,
          ips: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = IpHistoryController;
