const Module = require("../db/schema").module;
const Teacher = require("../db/schema").teacher;
class ModuleController {
  async createModule(req, res) {
    let module = new Module({
      name: req.body.name,
      subject: req.body.subject,
      resources: req.body.resources,
      facultyId: req.body.facultyId
    });

    let m = await module
      .save()
      .then(async docs => {
        let teacher = await Teacher.findOne({
          userid: req.body.facultyId
        }).then(teacher => {
          teacher.modules.push(docs._id);
          teacher.save(function(err, docs) {
            return true;
          });
        });
        return res.status(200).send({
          status: true,
          _id: docs.id,
          message: "Module Created"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Module Creation failed"
        });
      });
  }

  async getAllModule(req, res) {
    let m = await Module.find({})
      .populate("resources")
      .populate("facultyId")
      .then(docs => {
        return res.status(200).send({
          status: true,
          modules: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          modules: []
        });
      });
  }
  async getAllModuleById(req, res) {
    let m = await Module.findById(req.params.id)
      .populate("resources")
      .populate("facultyId")
      .then(docs => {
        return res.status(200).send({
          status: true,
          module: docs
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          module: []
        });
      });
  }

  async updateModule(req, res) {
    Module.findByIdAndUpdate(req.body.id, {
      $set: {
        name: req.body.name,
        subject: req.body.subject,
        resources: req.body.resources,
        facultyId: req.body.facultyId,
        updated_at: `${Date()}`
      }
    })
      .then(docs => {
        return res.status(200).send({
          status: true,
          message: "Module Updated!"
        });
      })
      .catch(err => {
        return res.status(400).send({
          status: false,
          message: "Network Error!"
        });
      });
  }
}
module.exports = ModuleController;
