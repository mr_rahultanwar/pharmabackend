const exportRoutes = [];

let routes = [
  require("./routes/user.routes"),
  require("./routes/course.routes"),
  require("./routes/messages.routes"),
  require("./routes/module.routes"),
  require("./routes/resource.routes"),
  require("./routes/calender.routes"),
  require("./routes/iphistory.routes"),
  require("./routes/question.routes"),
  require("./routes/questionbank.routes"),
  require("./routes/teachers.routes"),
  require("./routes/student.routes"),
  require("./routes/notice.routes"),
  require("./routes/exam.routes"),
  require("./routes/fileupload.routes"),
  require("./routes/result.routes"),
  require("./routes/notification.routes"),


];

routes.filter(route => {
  exportRoutes.push(...route);
});

module.exports = exportRoutes;
